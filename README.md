# Requirements
## On Windows
- wsl version2
- Virtualbox
## in wsl
- vagrant (best use their mirror)
- ansible
- working ssh-agent and key
- these environment variables, make sure paths are correct
```bash
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS='1'
export VAGRANT_WSL_WINDOWS_ACCESS_USER_HOME_PATH='/mnt/c/Users/foobar/'
export PATH="$PATH:/mnt/C/Program Files/Oracle/VirtualBox"
```
# Reference
also see Vagrantfile for comments

```bash
# if file does not exist
EDITOR=vim ansible-vault create group_vars/foo/bar.yaml
EDITOR=vim ansible-vault edit group_vars/foo/bar.yaml

vagrant up
# runs ansible on vm
vagrant provision 
# runs on production
ansible-playbook playbook.yaml
```
